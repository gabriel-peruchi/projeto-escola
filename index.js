const express = require('express')
const server = express()
const cors = require('cors')
const Usuario = require('./api/usuario/models/Usuario')

const routes = {
    dashboard: require('./api/dashboard/DashboardCtrl'),
    disciplina: require('./api/disciplina/DisciplinaCtrl'),
    estudante: require('./api/estudante/EstudanteCtrl'),
    professor: require('./api/professor/ProfessorCtrl'),
    turma: require('./api/turma/TurmaCtrl'),
    matricula: require('./api/matricula/MatriculaCtrl'),
    authentication: require('./api/authentication/AuthenticationCtrl'),
}

server.listen(process.env.PORT || 3000)
server.use(express.json())
server.use(cors())
server.use('/auth', routes.authentication)

server.use(async (req, res, next) => {
    try {
        const bearerToken = req.headers.authorization || ''
        const token = bearerToken.replace('Bearer ', '')

        const usuario = await Usuario.findOne({ token })

        if (!usuario) {
            const error = new Error()
            error.name = 'Unauthorized'
            throw error
        }

        next()
    } catch (error) {
        next(error)
    }
})

server.use('/dashboard', routes.dashboard)
server.use('/disciplinas', routes.disciplina)
server.use('/estudantes', routes.estudante)
server.use('/professores', routes.professor)
server.use('/turmas', routes.turma)
server.use('/matriculas', routes.matricula)

server.use((err, req, res, next) => {

    console.error(err.message)

    const error = {}

    switch (err.name) {
        case "Unauthorized": {
            error.code = 401
            error.message = "Não Autorizado"
            error.name = err.name
            break
        }

        case "ValidationError": {
            error.code = 400
            error.message = err.message
            error.name = err.name
            break
        }

        case "CastError": {
            error.code = 400
            error.message = err.message
            error.name = err.name
            break
        }

        default: {
            error.code = 500
            error.message = "Internal Error"
            error.name = "Internal Error"
            break
        }
    }

    res.status(error.code).json(error)
})
