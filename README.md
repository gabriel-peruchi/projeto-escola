## Documentação API - PROJETO ESCOLA
Esta documentação tem por finalidade auxiliar a integração entre APIs e facilitar a usabilidade do PROJETO ESCOLA. Esta aplicação tem por finalidade manipular e registrar as principais funcionalidas envolvendo o contexto de uma escola. Portanto, sua arquitetura é composta por **disciplina**, **professor**, **estudante**, **turma** e **matricula**.

### Endpoint
O endpoint de conexão com a API é: http://localhost:3000

### Recursos disponíveis
Atualmente existem os seguintes recursos abaixo que você pode manipular através dos métodos GET, POST, PUT e DELETE:
* Disciplina
* Professor
* Estudante
* Matricula
* Turma

### Tratamento de dados
Todos os dados enviados e recebidos pela API estão/deverão ser em formato JSON (application/json).

### Autenticação
Para poder trabalhar com os métodos disponíveis e assim fazer a integração, você precisa realizar o login com algum dos usuários pré-cadastrados no banco de dados MongoDB, para assim conseguir acessar o token de autenticação do seu usuário.

### Códigos de retorno
Na tabela abaixo estão listados os códigos de retorno das APIS.

| Código | Status                     |
| ------ | -------------------------- |
| 500    | Erro causado pelo servidor |
| 400    | Erro causado pelo cliente  |
| 401    | Não autorizado             |
| 200    | Sucesso                    |

## APIS DISPONÍVEIS

### DISCIPLINA 
 Este módulo é resposánvel por controlar as diciplinas

 ##### Parâmetros de chamada
| Configuração  | Valor                                                                             | 
| ------------- |-----------------------------------------------------------------------------------| 
| Authorization | **Bearer Token**: <token do usuário>|
| Headers       | **Content-type**: application/json                                                |

##### BUSCAR DISCIPLINAS - @GET
 ```url
 http://localhost:3000/disciplinas
 ```
Listar todas as disciplinas: `http://localhost:3000/disciplinas`

Listar disciplina única: `http://localhost:3000/disciplinas/:id`

###### QueryString disponíveis
| Parâmetro      | Descrição                                                                                         | 
| -------------  |---------------------------------------------------------------------------------------------------| 
| term           | Pesquia em regex pelo nome da disciplina                                                          | 
| limit          | Limita a quantidade de retorno das disciplinas                                                    | 

Exemplo de pesquisa com querystring: ```http://localhost:3000/disciplinas?term=`matematica``

##### SALVAR DISCIPLINA - @POST
  ```url
 http://localhost:3000/disciplinas
 ```
 
| Atributo        | Tipo   | Obrigatório | 
| --------------- |--------|:-----------:| 
| nome            | String | Sim         |

##### ALTERAR DISCIPLINA - @PUT
```url
http://localhost:3000/disciplinas/:id
```
Para alterar uma disciplina você precisa informar na URL do endpoint um {id} válido.

| Atributo        | Tipo   | Obrigatório |
| --------------- |--------|:-----------:|
| name            | String | Sim         |

##### EXCLUIR DISCIPLINA - @DELETE
```url
http://localhost:3000/disciplinas/:id
```
Para excluir uma disciplina você precisa informar na URL do endpoint um {id} válido.

###### =====================================================================================

### PROFESSOR
 Este módulo é resposánvel por controlar os professores da aplicação

##### Parâmetros de chamada
| Configuração  | Valor                                                                             | 
| ------------- |-----------------------------------------------------------------------------------| 
| Authorization | **Bearer Token**: <token do usuário>                                              |
| Headers       | **Content-type**: application/json                                                |

##### BUSCAR PROFESSORES - @GET
 ```url
 http://localhost:3000/professores
 ```
Listar todos os professores: `http://localhost:3000/professores`

Listar professor único: `http://localhost:3000/professores/:id`

###### QueryString disponíveis
| Parâmetro      | Descrição                                                                                         | 
| -------------  |---------------------------------------------------------------------------------------------------| 
| term           | Pesquisa regex por nome, sobrenome, cpf e email                                                   | 
| grauAcademico  | Pesquisa pelo grau academico do professor                                                         | 
| limit          | Limita a quantidade de retorno dos professores                                                     |

Exemplo de pesquisa com querystring: `http://localhost:3000/professores?term=joao&grauAcademico=MESTRADO`

##### SALVAR PROFESSOR - @POST
  ```url
 http://localhost:3000/professores
 ```
 
| Atributo        | Tipo    | Obrigatório |
| --------------- |---------|:-----------:| 
| nome            | String  | Sim         |
| sobrenome       | String  | Sim         |
| cpf             | String  | Sim         |
| dataNascimento  | Date    | Sim         |
| email           | String  | Sim         |
| celular         | String  | Sim         |
| telefoneFixo    | String  | Não         |
| grauAcademico   | String  | Sim         |
| experiencia     | String  | Sim         |

##### ALTERAR PROFESSOR - @PUT
```url
http://localhost:3000/professores/:id
```
Para alterar um professor você precisa informar na URL do endpoint um {id} válido.

| Atributo        | Tipo    | Obrigatório |
| --------------- |---------|:-----------:| 
| nome            | String  | Sim         |
| sobrenome       | String  | Sim         |
| cpf             | String  | Sim         |
| dataNascimento  | Date    | Sim         |
| email           | String  | Sim         |
| celular         | String  | Sim         |
| telefoneFixo    | String  | Não         |
| grauAcademico   | String  | Sim         |
| experiencia     | String  | Sim         |

##### EXCLUIR PROFESSOR - @DELETE
```url
http://localhost:3000/professores/:id
```
Para excluir um professor você precisa informar na URL do endpoint um {id} válido.

###### =====================================================================================

### ESTUDANTE
 Este módulo é resposánvel por controlar os estudantes da aplicação

##### Parâmetros de chamada
| Configuração  | Valor                                                                             | 
| ------------- |-----------------------------------------------------------------------------------| 
| Authorization | **Bearer Token**: <token do usuário>                                              |
| Headers       | **Content-type**: application/json                                                |

##### BUSCAR ESTUDANTES - @GET
 ```url
 http://localhost:3000/estudantes
 ```
Listar todosos estudantes: `http://localhost:3000/estudantes`

Listar estudante único: `http://localhost:3000/estudantes/:id`

###### QueryString disponíveis
| Parâmetro      | Descrição                                                                                         | 
| -------------  |---------------------------------------------------------------------------------------------------| 
| term           | Pesquisa regex por nome, sobrenome, cpf e email                                                   | 
| limit          | Limita a quantidade de retorno dos estudantes                                                     | 

Exemplo de pesquisa com querystring: `http://localhost:3000/estudantes?term=pedro&limit=10`

##### SALVAR ESTUDANTE - @POST
  ```url
 http://localhost:3000/estudantes
```

| Atributo        | Tipo    | Obrigatório |
| --------------- |---------|:-----------:| 
| nome            | String  | Sim         |
| sobrenome       | String  | Sim         |
| cpf             | String  | Sim         |
| dataNascimento  | Date    | Sim         |
| email           | String  | Sim         |
| celular         | String  | Sim         |
| telefoneFixo    | String  | Não         |

##### ALTERAR ESTUDANTE - @PUT
```url
http://localhost:3000/estudantes/:id
```
Para alterar um estudante você precisa informar na URL do endpoint um {id} válido.

| Atributo        | Tipo    | Obrigatório |
| --------------- |---------|:-----------:| 
| nome            | String  | Sim         |
| sobrenome       | String  | Sim         |
| cpf             | String  | Sim         |
| dataNascimento  | Date    | Sim         |
| email           | String  | Sim         |
| celular         | String  | Sim         |
| telefoneFixo    | String  | Não         |

##### EXCLUIR ESTUDANTE - @DELETE
```url
http://localhost:3000/estudantes/:id
```
Para excluir um estudante você precisa informar na URL do endpoint um {id} válido.

###### =====================================================================================

### TURMA
 Este módulo é resposánvel por controlar as turmas da aplicação

##### Parâmetros de chamada
| Configuração  | Valor                                                                             | 
| ------------- |-----------------------------------------------------------------------------------| 
| Authorization | **Bearer Token**: <token do usuário>|
| Headers       | **Content-type**: application/json                                                |

##### BUSCAR TURMAS - @GET
 ```url
 http://localhost:3000/turmas
 ```
Listar todas as turmas: `http://localhost:3000/turmas`

Listar turma única: `http://localhost:3000/turmas/:id`

###### QueryString disponíveis
| Parâmetro      | Descrição                                                                                         | 
| -------------  |---------------------------------------------------------------------------------------------------| 
| term           | Pesquisa em regex pelo nome da turma                                                              | 
| limit          | Limita a quantidade de retorno das turmas                                                         | 

Exemplo de pesquisa com querystring: `http://localhost:3000/turmas?nome=3°`

##### SALVAR TURMA - @POST
  ```url
 http://localhost:3000/turmas
```

| Atributo        | Tipo     | Obrigatório | 
| --------------- |--------- |:-----------:| 
| nome            | String   | Sim         |
| professor       | ObjectId | Sim         |
| disciplina      | ObjectId | Sim         |

##### ALTERAR TURMA - @PUT
```url
http://localhost:3000/turmas/:id
```
Para alterar uma turma você precisa informar na URL do endpoint um {id} válido.

| Atributo        | Tipo     | Obrigatório | 
| --------------- |--------- |:-----------:| 
| nome            | String   | Sim         |
| professor       | ObjectId | Sim         |
| disciplina      | ObjectId | Sim         |

##### EXCLUIR TURMA - @DELETE
```url
http://localhost:3000/turmas/:id
```
Para excluir uma turma você precisa informar na URL do endpoint um {id} válido.

###### =====================================================================================

### MATRICULA
 Este módulo é resposánvel por controlar as matriculas da aplicação

##### Parâmetros de chamada
| Configuração  | Valor                                                                             | 
| ------------- |-----------------------------------------------------------------------------------| 
| Authorization | **Bearer Token**: <token do usuário>                                              |
| Headers       | **Content-type**: application/json                                                |

##### BUSCAR MATRICULAS - @GET
 ```url
 http://localhost:3005/matriculas
 ```
Listar todas as matriculas: `http://localhost:3005/matriculas`

Listar matricula única: `http://localhost:3005/matriculas/:id`

###### QueryString disponíveis
| Parâmetro      | Descrição                                                                                         | 
| -------------  |---------------------------------------------------------------------------------------------------| 
| dataInicio     | Pesquisa pela data da matricula no periodo entre a dataInicio e dataFim                           | 
| dataFim        | Pesquisa pela data da matricula no periodo entre a dataInicio e dataFim                           | 
| limit          | Limita a quantidade de retorno das matriculas                                                     | 

Exemplo de pesquisa com querystring: `http://localhost:3005/matriculas?dataInicio=2020-10-01&dataFim=2020-10-31`

##### SALVAR MATRICULA - @POST
  ```url
 http://localhost:3005/matriculas
```

| Atributo        | Tipo     | Obrigatório | 
| --------------- |--------- |:-----------:| 
| estudante       | ObjectId | Sim         |
| turma           | ObjectId | Sim         |
| dataMatricula   | Date     | Sim         |

##### ALTERAR MATRICULA - @PUT
```url
http://localhost:3005/matriculas/:id
```
Para alterar uma matricula você precisa informar na URL do endpoint um {id} válido.

| Atributo        | Tipo     | Obrigatório | 
| --------------- |--------- |:-----------:| 
| estudante       | ObjectId | Sim         |
| turma           | ObjectId | Sim         |
| dataMatricula   | Date     | Sim         |

##### EXCLUIR MATRICULA - @DELETE
```url
http://localhost:3005/matriculas/:id
```
Para excluir uma matricula você precisa informar na URL do endpoint um {id} válido.