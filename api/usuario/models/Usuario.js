const mongoose = require('../../data')

const usuarioSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    senha: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: false
    }
})

const Usuario = new mongoose.model("Usuario", usuarioSchema)

module.exports = Usuario