const express = require('express')
const Router = express.Router()
const Usuario = require('../usuario/models/Usuario')

Router.post('/', async (req, res, next) => {
    try {
        const { email, senha } = req.body

        const usuario = await Usuario.findOne({ email, senha })

        if (!usuario) {
            const error = new Error()
            error.name = "Unauthorized"
            throw error
        }

        res.status(200).json(usuario)
    } catch (error) {
        next(error)
    }
})

module.exports = Router