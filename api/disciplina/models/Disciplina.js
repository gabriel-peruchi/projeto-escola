const mongoose = require('../../data')

const disciplinaSchema = new mongoose.Schema({
    nome: {
        type: String,
        required: true
    },
    ativo: {
        type: Boolean,
        default: true
    }
})

const Discplina = new mongoose.model("Disciplina", disciplinaSchema)

module.exports = Discplina