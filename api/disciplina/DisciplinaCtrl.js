const express = require('express')
const Router = express.Router()
const Disciplina = require('./models/Disciplina')

Router.get('/', async (req, res, next) => {
    try {
        const limit = Number(req.query.limit) || 5
        const term = req.query.term

        const options = {}

        if (term) {
            options.nome = { $regex: `.*${term}.*`, $options: 'i' }
        }

        const disciplinas = await Disciplina
            .find({ ativo: true, ...options })
            .sort({ nome: 1 })
            .limit(limit)

        res.status(200).json(disciplinas)
    } catch (error) {
        next(error)
    }
})

Router.get('/:id', async (req, res, next) => {
    try {
        const idDisciplina = req.params.id

        const disciplina = await Disciplina.findById(idDisciplina)

        res.status(200).json(disciplina)
    } catch (error) {
        next(error)
    }
})

Router.post('/', async (req, res, next) => {
    try {
        const disciplina = new Disciplina(req.body)

        const result = await disciplina.save()

        res.status(200).json(result)
    } catch (error) {
        next(error)
    }
})

Router.put('/:id', async (req, res, next) => {
    try {
        const idDisciplina = req.params.id
        const paramsUpdate = req.body

        await Disciplina.updateOne({ _id: idDisciplina }, { ...paramsUpdate })

        res.status(200).json({ message: "Atualizado com Sucesso!" })
    } catch (error) {
        next(error)
    }
})

Router.delete('/:id', async (req, res, next) => {
    try {
        const idDisciplina = req.params.id

        await Disciplina.updateOne({ _id: idDisciplina }, { ativo: false })

        res.status(200).json({ message: "Excluído com Sucesso!" })
    } catch (error) {
        next(error)
    }
})

module.exports = Router