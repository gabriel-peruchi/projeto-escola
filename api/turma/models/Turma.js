const mongoose = require('../../data')

const turmaSchema = new mongoose.Schema({
    nome: {
        type: String,
        required: true
    },
    professor: {
        type: mongoose.Schema.ObjectId,
        ref: 'Professor',
        required: true
    },
    disciplina: {
        type: mongoose.Schema.ObjectId,
        ref: 'Disciplina',
        required: true
    },
    ativo: {
        type: Boolean,
        default: true
    }
})

const Turma = new mongoose.model("Turma", turmaSchema)

module.exports = Turma