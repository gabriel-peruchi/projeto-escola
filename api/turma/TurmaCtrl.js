const express = require('express')
const Router = express.Router()
const Turma = require('./models/Turma')

Router.get('/', async (req, res, next) => {
    try {
        const limit = Number(req.query.limit) || 5
        const term = req.query.term

        const options = {}

        if (term) {
            options.$or = [
                { nome: { $regex: `.*${term}.*`, $options: 'i' } },
                { 'professor.nome': { $regex: `.*${term}.*`, $options: 'i' } },
                { 'disciplina.nome': { $regex: `.*${term}.*`, $options: 'i' } }
            ]
        }

        const turmas = await Turma.aggregate([
            {
                $match: { ativo: true }
            },
            {
                $lookup: {
                    from: 'professores',
                    localField: 'professor',
                    foreignField: '_id',
                    as: 'professor'
                }
            },
            {
                $lookup: {
                    from: 'disciplinas',
                    localField: 'disciplina',
                    foreignField: '_id',
                    as: 'disciplina'
                }
            },
            {
                $unwind: {
                    path: '$professor',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: {
                    path: '$disciplina',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $match: options
            },
            {
                $sort: { nome: 1 }
            },
        ]).limit(limit)

        res.status(200).json(turmas)
    } catch (error) {
        next(error)
    }
})

Router.get('/:id', async (req, res, next) => {
    try {
        const idTurma = req.params.id

        const turma = await Turma.findById(idTurma)
            .populate('professor')
            .populate('disciplina')

        res.status(200).json(turma)
    } catch (error) {
        next(error)
    }
})

Router.post('/', async (req, res, next) => {
    try {
        const turma = new Turma(req.body)

        const result = await turma.save()

        res.status(200).json(result)
    } catch (error) {
        next(error)
    }
})

Router.put('/:id', async (req, res, next) => {
    try {
        const idTurma = req.params.id
        const paramsUpdate = req.body

        await Turma.updateOne({ _id: idTurma }, { ...paramsUpdate })

        res.status(200).json({ message: "Atualizada com Sucesso!" })
    } catch (error) {
        next(error)
    }
})

Router.delete('/:id', async (req, res, next) => {
    try {
        const idTurma = req.params.id

        await Turma.updateOne({ _id: idTurma }, { ativo: false })

        res.status(200).json({ message: "Excluída com Sucesso!" })
    } catch (error) {
        next(error)
    }
})

module.exports = Router