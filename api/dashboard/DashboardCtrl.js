const express = require('express')
const Router = express.Router()
const Turma = require('../turma/models/Turma')

Router.get('/', async (req, res, next) => {
    try {
        const turmas = await Turma.aggregate([
            {
                $match: { ativo: true }
            },
            {
                $lookup: {
                    from: 'professores',
                    localField: 'professor',
                    foreignField: '_id',
                    as: 'professor'
                }
            },
            {
                $unwind: {
                    path: '$professor',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from: 'disciplinas',
                    localField: 'disciplina',
                    foreignField: '_id',
                    as: 'disciplina'
                }
            },
            {
                $unwind: {
                    path: '$disciplina',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from: 'matriculas',
                    localField: '_id',
                    foreignField: 'turma',
                    as: 'matriculas'
                }
            },
            {
                $lookup: {
                    from: 'estudantes',
                    localField: 'matriculas.estudante',
                    foreignField: '_id',
                    as: 'estudantes'
                }
            },
            {
                $sort: { nome: 1 }
            },
            {
                $project: {
                    nome: true,
                    professor: { nome: true, sobrenome: true },
                    disciplina: { nome: true },
                    qtdMatriculas: { $size: '$matriculas' },
                    estudantes: { nome: true, sobrenome: true },
                }
            }
        ])

        res.status(200).json(turmas)
    } catch (error) {
        next(error)
    }
})

module.exports = Router