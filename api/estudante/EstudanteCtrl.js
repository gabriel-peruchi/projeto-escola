const express = require('express')
const Router = express.Router()
const Estudante = require('./models/Estudante')

Router.get('/', async (req, res, next) => {
    try {
        const limit = Number(req.query.limit) || 5
        const term = req.query.term

        const options = {}

        if (term) {
            options.$or = [
                { nome: { $regex: `.*${term}.*`, $options: 'i' } },
                { sobrenome: { $regex: `.*${term}.*`, $options: 'i' } },
                { cpf: { $regex: `.*${term}.*`, $options: 'i' } },
                { email: { $regex: `.*${term}.*`, $options: 'i' } },
            ]
        }

        const estudantes = await Estudante
            .find({ ativo: true, ...options })
            .sort({ nome: 1 })
            .limit(limit)

        res.status(200).json(estudantes)
    } catch (error) {
        next(error)
    }
})

Router.get('/:id', async (req, res, next) => {
    try {
        const idEstudante = req.params.id

        const estudante = await Estudante.findById(idEstudante)

        res.status(200).json(estudante)
    } catch (error) {
        next(error)
    }
})

Router.post('/', async (req, res, next) => {
    try {
        const estudante = new Estudante(req.body)

        const result = await estudante.save()

        res.status(200).json(result)
    } catch (error) {
        next(error)
    }
})

Router.put('/:id', async (req, res, next) => {
    try {
        const idEstudante = req.params.id
        const paramsUpdate = req.body

        await Estudante.updateOne({ _id: idEstudante }, { ...paramsUpdate })

        res.status(200).json({ message: "Atualizado com Sucesso!" })
    } catch (error) {
        next(error)
    }
})

Router.delete('/:id', async (req, res, next) => {
    try {
        const idEstudante = req.params.id

        await Estudante.updateOne({ _id: idEstudante }, { ativo: false })

        res.status(200).json({ message: "Excluído com Sucesso!" })
    } catch (error) {
        next(error)
    }
})

module.exports = Router