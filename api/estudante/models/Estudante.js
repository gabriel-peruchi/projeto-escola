const mongoose = require('../../data')

const estudanteSchema = new mongoose.Schema({
    nome: {
        type: String,
        required: true
    },
    sobrenome: {
        type: String,
        required: true
    },
    cpf: {
        type: String,
        required: true
    },
    dataNascimento: {
        type: Date,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    celular: {
        type: String,
        required: true
    },
    telefoneFixo: {
        type: String,
        required: false
    },
    ativo: {
        type: Boolean,
        default: true
    }
})

const Estudante = new mongoose.model("Estudante", estudanteSchema)

module.exports = Estudante