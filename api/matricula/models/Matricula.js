const mongoose = require('../../data')

const matriculaSchema = new mongoose.Schema({
    estudante: {
        type: mongoose.Schema.ObjectId,
        ref: 'Estudante',
        required: true
    },
    turma: {
        type: mongoose.Schema.ObjectId,
        ref: 'Turma',
        required: true
    },
    dataMatricula: {
        type: Date,
        default: new Date()
    },
    ativo: {
        type: Boolean,
        default: true
    }
})

const Matricula = new mongoose.model("Matricula", matriculaSchema)

module.exports = Matricula