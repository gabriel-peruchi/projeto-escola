const express = require('express')
const Router = express.Router()
const Matricula = require('./models/Matricula')

Router.get('/', async (req, res, next) => {
    try {
        const limit = Number(req.query.limit) || 5
        const term = req.query.term
        const dataInicio = req.query.dataInicio
        const dataFim = req.query.dataFim

        const options = {}

        if (term) {
            options.$or = [
                { 'estudante.nome': { $regex: `.*${term}.*`, $options: 'i' } },
                { 'turma.nome': { $regex: `.*${term}.*`, $options: 'i' } }
            ]
        }

        if (dataInicio && dataFim) {
            const dataInicioUTC = new Date(dataInicio)
            const dataFimUTC = new Date(dataFim)

            dataInicioUTC.setUTCHours(0, 0, 1)
            dataFimUTC.setUTCHours(23, 59, 59)

            options.dataMatricula = {
                $gte: new Date(dataInicioUTC),
                $lte: new Date(dataFimUTC)
            }
        }

        const matriculas = await Matricula.aggregate([
            {
                $match: { ativo: true }
            },
            {
                $lookup: {
                    from: 'estudantes',
                    localField: 'estudante',
                    foreignField: '_id',
                    as: 'estudante'
                }
            },
            {
                $lookup: {
                    from: 'turmas',
                    localField: 'turma',
                    foreignField: '_id',
                    as: 'turma'
                }
            },
            {
                $unwind: {
                    path: '$estudante',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: {
                    path: '$turma',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $match: options
            },
            {
                $sort: { dataMatricula: -1 }
            }
        ]).limit(limit)

        res.status(200).json(matriculas)
    } catch (error) {
        next(error)
    }
})

Router.get('/:id', async (req, res, next) => {
    try {
        const idMatricula = req.params.id

        const matricula = await Matricula.findById(idMatricula)
            .populate('estudante')
            .populate('turma')

        res.status(200).json(matricula)
    } catch (error) {
        next(error)
    }
})

Router.post('/', async (req, res, next) => {
    try {
        const matricula = new Matricula(req.body)

        const result = await matricula.save()

        res.status(200).json(result)
    } catch (error) {
        next(error)
    }
})

Router.put('/:id', async (req, res, next) => {
    try {
        const idMatricula = req.params.id
        const paramsUpdate = req.body

        await Matricula.updateOne({ _id: idMatricula }, { ...paramsUpdate })

        res.status(200).json({ message: "Atualizada com Sucesso!" })
    } catch (error) {
        next(error)
    }
})

Router.delete('/:id', async (req, res, next) => {
    try {
        const idMatricula = req.params.id

        await Matricula.updateOne({ _id: idMatricula }, { ativo: false })

        res.status(200).json({ message: "Excluída com Sucesso!" })
    } catch (error) {
        next(error)
    }
})

module.exports = Router