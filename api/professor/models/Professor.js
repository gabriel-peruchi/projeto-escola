const mongoose = require('../../data')
const GrauAcademicoEnum = require('./GrauAcademicoEnum')

const professorSchema = new mongoose.Schema({
    nome: {
        type: String,
        required: true
    },
    sobrenome: {
        type: String,
        required: true
    },
    cpf: {
        type: String,
        required: true
    },
    dataNascimento: {
        type: Date,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    celular: {
        type: String,
        required: true
    },
    telefoneFixo: {
        type: String,
        required: false
    },
    grauAcademico: {
        type: GrauAcademicoEnum,
        required: true
    },
    experiencia: {
        type: String,
        required: true
    },
    ativo: {
        type: Boolean,
        default: true
    }
})

const Professor = new mongoose.model("Professor", professorSchema, "professores")

module.exports = Professor