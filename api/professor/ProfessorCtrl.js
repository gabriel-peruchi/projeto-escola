const express = require('express')
const Router = express.Router()
const Professor = require('./models/Professor')

Router.get('/', async (req, res, next) => {
    try {
        const limit = Number(req.query.limit) || 5
        const term = req.query.term
        const grauAcademico = req.query.grauAcademico

        const options = {}

        if (term) {
            options.$or = [
                { nome: { $regex: `.*${term}.*`, $options: 'i' } },
                { sobrenome: { $regex: `.*${term}.*`, $options: 'i' } },
                { cpf: { $regex: `.*${term}.*`, $options: 'i' } },
                { email: { $regex: `.*${term}.*`, $options: 'i' } },
            ]
        }

        if (grauAcademico) {
            options.grauAcademico = grauAcademico
        }

        const professores = await Professor
            .find({ ativo: true, ...options })
            .sort({ nome: 1 })
            .limit(limit)

        res.status(200).json(professores)
    } catch (error) {
        next(error)
    }
})

Router.get('/:id', async (req, res, next) => {
    try {
        const idProfessor = req.params.id

        const professor = await Professor.findById(idProfessor)

        res.status(200).json(professor)
    } catch (error) {
        next(error)
    }
})

Router.post('/', async (req, res, next) => {
    try {
        const professor = new Professor(req.body)

        const result = await professor.save()

        res.status(200).json(result)
    } catch (error) {
        next(error)
    }
})

Router.put('/:id', async (req, res, next) => {
    try {
        const idProfessor = req.params.id
        const paramsUpdate = req.body

        await Professor.updateOne({ _id: idProfessor }, { ...paramsUpdate })

        res.status(200).json({ message: "Atualizado com Sucesso!" })
    } catch (error) {
        next(error)
    }
})

Router.delete('/:id', async (req, res, next) => {
    try {
        const idProfessor = req.params.id

        await Professor.updateOne({ _id: idProfessor }, { ativo: false })

        res.status(200).json({ message: "Excluído com Sucesso!" })
    } catch (error) {
        next(error)
    }
})

module.exports = Router