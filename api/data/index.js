const mongoose = require('mongoose')
const config = require('../../config.json')

const URL = `mongodb+srv://${config.user}:${config.password}@cluster0.oeugf.mongodb.net/${config.database}?retryWrites=true&w=majority`

mongoose.connect(URL, { useNewUrlParser: true, useUnifiedTopology: true })

module.exports = mongoose